#!/bin/perl

use warnings;
use strict;

if($#ARGV < 0){
    print STDERR "Usage\n1:File 1\n2:Starting column number\n3:Ending column number\nThis prints the distances between adjacent coefficients. Run rmse code with the output of those for original and predicted";
    exit;
}

my $err=0;
open(Inp1,"<$ARGV[0]") || die ("Error opening file");
my $x1=$ARGV[1];
my $x2=$ARGV[2];
my $count=0;
my @lin1;
my $j;

my @File1=<Inp1>;

my $y1=@File1; 

for(my $i=0; $i < $y1; $i++){
    $File1[$i]=~ s/^\s+//; #Leading white space screws up the way I use split()
    @lin1=split(/\s+/,$File1[$i]);
    if(!defined($lin1[$x2-1])){
	print STDERR "Stated range exceeds dimensions";
	exit;
    }
    for($j=$x1;$j < $x2; $j++){
	$err=$lin1[$j]-$lin1[$j-1];#abs() is probably incorrect here
	print "$err ";
    }
    print "\n";
}
