#!/bin/perl

use warnings;

if($#ARGV < 0){
    print STDERR "Usage\n1:File 1\n2:File 2\n3:Starting column number\n4:Ending column number\n";
    exit;
}

my $err=0;
open(Inp1,"<$ARGV[0]") || die ("File 1 not found");
open(Inp2,"<$ARGV[1]") || die ("File 2 not found");
my $x1=$ARGV[2];
my $x2=$ARGV[3];
my $count=0;

@File1=<Inp1>;
@File2=<Inp2>;

#print "@File1[1..2]";
#print "@File2[1..2]";

$y1=@File1; $y2=@File2;
my $len=$y1;
if($y1 != $y2){
    print STDERR "Warning: File lengths unequal !\nRMSE will only be computed upto size of smaller file\n\n";
    if($y1 < $y2){
        $len=$y1;
    }
    else{
        $len=$y2;
    }
}

for($i=0; $i < $len; $i++){
    $File1[$i]=~ s/^\s+//;
    $File2[$i]=~ s/^\s+//;
    @lin1=split(/\s+/,$File1[$i]);
    @lin2=split(/\s+/,$File2[$i]);
    if(!defined($lin1[$x2-1]) || !defined($lin2[$x2-1])){
	print STDERR "Stated range exceeds dimensions\n";
	exit;
    }
    for($j=($x1-1) ;$j < $x2; $j++){
	$err=$err+((abs($lin1[$j]-$lin2[$j]))**2);
	$count++;
    }
}

$err=$err/$count;
$err=sqrt($err);
close(Inp1);
close(Inp2);

print "$err\n";


